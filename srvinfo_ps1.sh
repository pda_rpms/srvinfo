# http://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt
if [ "$PS1" ] && [ "$TERM" ]; then
    if [ -r /usr/lib/libsrvinfo.sh ]; then
        source /usr/lib/libsrvinfo.sh

        case $TERM in
	    xterm*|vte*)
		case $SRVROLE in
		    production|internal|stagging)
			PSTPL=$SR_COLOR_256
		    ;;
		    *)
			PSTPL=""
		    ;;
		esac
		PSTPL="${PSTPL}\u@\h\[\e[0m\]\[\e[01;38;5;250m\][\[\e[01;38;5;080m\]\W\[\e[0m\]\[\e[01;38;5;250m\]]\[\e[0m\]"
		if [ "$EUID" -eq 0 ]; then
		    PSTPL="${PSTPL}\[\e[01;38;5;160m\]\\$\[\e[0m\] "
		else
		    PSTPL="${PSTPL}\[\e[01;38;5;046m\]\\$\[\e[0m\] "
		fi
	    ;;
	    linux)
		case $SRVROLE in
		    production|internal|stagging)
			PSTPL=$SR_COLOR_16
		    ;;
		    *)
			PSTPL=""
		    ;;
		esac
		PSTPL="${PSTPL}\u@\h\[\e[0m\][\[\e[0;36m\]\W\[\e[0m\]]"
		if [ "$EUID" -eq 0 ]; then
		    PSTPL="${PSTPL}\[\e[0;31m\]\\$\[\e[0m\] "
		else
		    PSTPL="${PSTPL}\[\e[0;32m\]\\$\[\e[0m\] "
		fi
    	    ;;
    	    *)
    		PSTPL=$PS1
    	    ;;
	esac

	PS1=$PSTPL
	unset -v SRVROLE PSTPL SR_COLOR_256 SR_UCOLOR_256 SR_COLOR_16 SR_UCOLOR_16
    fi
fi
