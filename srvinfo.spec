Name:		srvinfo
Summary:	Personal server summary script
Summary(ru_RU.UTF-8): Лчиный скрипт, предоставляющий сводку по серверу
Version:	1.0.0
Release:	1%{?dist}
License:	GPL
URL:		https://gitlab.com/pda_rpms/srvinfo
Group:		System Environment/Daemons
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	COPYING
Source1:	srvinfo.sh
Source2:	srvinfo_ps1.sh
Source3:	srvinfo.bsh
Source4:	libsrvinfo.sh
Source5:	php-mini.ini
Source6:	srvinfo
Source7:	srvbanner
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
Requires: bash bc gawk coreutils sed procps-ng grep which iproute

%description
A personal set of scripts that displays information about the load,
available memory and location, as well as installed software, upon
entering the server. The script also dyes the command line to make
it more difficult to confuse the debugging and the production server.

%description -l ru_RU.UTF-8
Личный набор скриптов, выводящий при входе на сервер информацию о загрузке,
доступной памяти и месте, а так же установленном програмном обеспечении.
Так же скрипт подсвечивает командную строку, чтобы сложнее было спутять
отладочный и "боевой" сервер.

%prep
%{__cp} %{SOURCE0} %{_builddir}/COPYING

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/profile.d/
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/profile.d/
%{__install} -pm 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/profile.d/

%{__mkdir_p} %{buildroot}%{_prefix}/bin
%{__install} -pm 0755 %{SOURCE3} %{buildroot}%{_prefix}/bin/%{name}.sh

%{__mkdir_p} %{buildroot}%{_prefix}/lib
%{__install} -pm 0644 %{SOURCE4} %{buildroot}%{_prefix}/lib/

%{__mkdir_p} %{buildroot}%{_prefix}/etc
%{__install} -pm 0644 %{SOURCE5} %{buildroot}%{_prefix}/etc/

%{__mkdir_p} %{buildroot}%{_sysconfdir}/sysconfig
%{__install} -pm 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/sysconfig/
%{__install} -pm 0644 %{SOURCE7} %{buildroot}%{_sysconfdir}/sysconfig/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%{__rm} %{_builddir}/COPYING

%files
%license COPYING
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/srvbanner
%{_sysconfdir}/profile.d/%{name}.sh
%{_sysconfdir}/profile.d/%{name}_ps1.sh
%{_prefix}/bin/%{name}.sh
%{_prefix}/lib/lib%{name}.sh
%{_prefix}/etc/php-mini.ini

%changelog
* Tue Dec 04 2018 Dmitriy Pomerantsev <pda2@yandex.ru> 1.0.0-1
- Initial package
