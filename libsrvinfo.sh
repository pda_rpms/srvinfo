[ -r /etc/sysconfig/srvinfo ] && source /etc/sysconfig/srvinfo || true

case $SRVROLE in
    production)
	SR_COLOR_256="\[\e[01;38;5;040m\]"
	SR_UCOLOR_256="\e[01;38;5;040m"
	SR_COLOR_16="\[\e[0;32m\]"
	SR_UCOLOR_16="\e[0;32m"
    ;;
    internal)
	SR_COLOR_256="\[\e[01;38;5;045m\]"
	SR_UCOLOR_256="\e[01;38;5;045m"
	SR_COLOR_16="\[\e[0;36m\]"
	SR_UCOLOR_16="\e[0;36m"
    ;;
    stagging)
	SR_COLOR_256="\[\e[01;38;5;190m\]"
	SR_UCOLOR_256="\e[01;38;5;190m"
	SR_COLOR_16="\[\e[0;33m\]"
	SR_UCOLOR_16="\e[0;33m"
    ;;
    *)
	SR_COLOR_256=""
	SR_UCOLOR_256=""
	SR_COLOR_16=""
	SR_UCOLOR_16=""
    ;;
esac
